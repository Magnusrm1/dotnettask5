﻿using System;
using System.Collections.Generic;

namespace DotNetTask5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> stable = new List<Animal>();
            stable.Add(new Animal("Hank"));
            stable.Add(new Animal("Henry"));
            stable.Add(new Animal("Helen", 6));

            stable[2].JumpHurdle();
            stable[1].JumpHurdle();
            stable[2].JumpHurdle();

            Console.WriteLine($"{stable[2].name} has jumped {stable[2].hurdlesJumped} hurdles.");

        }
    }
}

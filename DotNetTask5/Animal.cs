﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask5
{
    class Animal
    {
        public string name;
        public int hurdlesJumped;

        public Animal(string name)
        {
            this.name = name;
            this.hurdlesJumped = 0;
        }

        public Animal(string name, int hurdlesJumped)
        {
            this.name = name;
            this.hurdlesJumped = hurdlesJumped;
        }

        public string Name { get => name; set => name = value; }
        public int HurdlesJumped { get => hurdlesJumped; set => hurdlesJumped = value; }

        public void JumpHurdle() 
        {
            this.hurdlesJumped++;
            Console.WriteLine(this.name + " jumped a hurdle!");
        }

        public void JumpHurdle(int amount)
        {
            this.hurdlesJumped += amount;
            Console.WriteLine(this.name + " jumped " + amount + " hurdles!");
        }

    }
}
